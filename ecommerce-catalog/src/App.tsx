import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import CartStore from './stores/CartStore';
import CartObserver from './stores/CartObserver';
import ProductFactory from './factories/ProductFactory';
import ProductRepository from './repositories/ProductRepository';
import ProductForm from './components/ProductForm/ProductForm';
import CartList from './components/CartList/CartList';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Catalog from './components/Catalog/Catalog';
import EditProductForm from './components/EditProductForm/EditProductForm';

const productRepository = ProductRepository.getInstance();
const cartStore = CartStore.getInstance();

const App: React.FC = () => {
    const [products, setProducts] = useState<any[]>(productRepository.getAll());
    const [filteredProducts, setFilteredProducts] = useState<any[]>(productRepository.getAll());
    const [cartItems, setCartItems] = useState<{ product: any, quantity: number }[]>([]);
    const [selectedProduct, setSelectedProduct] = useState<any | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [priceRange, setPriceRange] = useState<{ min: number, max: number }>({ min: 0, max: 0 });
    const [searchQuery, setSearchQuery] = useState<string>('');

    useEffect(() => {
        fetch('http://localhost:5000/products')
            .then(response => response.json())
            .then(data => {
                productRepository.loadProducts(data);
                setProducts(data);
                setFilteredProducts(data);
                localStorage.setItem('products', JSON.stringify(data));
            });

        const savedCartItems = localStorage.getItem('cartItems');
        if (savedCartItems) {
            const parsedCartItems = JSON.parse(savedCartItems);
            setCartItems(parsedCartItems);
        }

        const updateState = () => {
            setCartItems(cartStore.getItems());
        };

        const observer = new CartObserver(updateState);
        cartStore.addObserver(observer);

        return () => {
            cartStore.removeObserver(observer);
        };
    }, []);

    const saveProducts = (updatedProducts: any[]) => {
        setProducts(updatedProducts);
        setFilteredProducts(updatedProducts);
        localStorage.setItem('products', JSON.stringify(updatedProducts));
    };

    const addProduct = (name: string, price: number, type: string) => {
        const duplicateProduct = products.find(product => product.name === name && product.type === type);
        if (duplicateProduct) {
            setError(`A product with the name "${name}" already exists in the "${type}" category.`);
            return;
        }

        const product = ProductFactory.createProduct(type, name, price);

        fetch('http://localhost:5000/products', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)
        })
        .then(response => response.json())
        .then(newProduct => {
            productRepository.add(newProduct);
            const updatedProducts = productRepository.getAll();
            saveProducts(updatedProducts);
            setError(null);
        });
    };

    const updateProduct = (id: string, name: string, price: number, type: string) => {
        const duplicateProduct = products.find(product => product.name === name && product.type === type && product.id !== id);
        if (duplicateProduct) {
            setError(`A product with the name "${name}" already exists in the "${type}" category.`);
            return;
        }

        const updatedProduct = { id, name, price, type };

        fetch(`http://localhost:5000/products/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedProduct)
        })
        .then(response => response.json())
        .then(() => {
            productRepository.loadProducts(productRepository.getAll().map(product => 
                product.id === id ? updatedProduct : product
            ));
            setSelectedProduct(null);
            setError(null);
        });
    };

    const removeProduct = (id: string) => {
        const productToRemove = productRepository.findById(id);
        if (productToRemove) {
            fetch(`http://localhost:5000/products/${id}`, {
                method: 'DELETE'
            })
            .then(() => {
                productRepository.remove(productToRemove);
                const updatedProducts = productRepository.getAll();
                saveProducts(updatedProducts);
            });
        }
    };

    const removeCartItem = (id: string) => {
        cartStore.removeItem(id);
    };

    const filterByType = (type: string) => {
        const filteredProducts = productRepository.findByType(type);
        setFilteredProducts(filteredProducts);
    };

    const filterByPriceRange = (min: number, max: number) => {
        const filteredProducts = productRepository.findByPriceRange(min, max);
        setFilteredProducts(filteredProducts);
    };

    const showAllProducts = () => {
        setFilteredProducts(products);
    };

    const handlePriceRangeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPriceRange({
            ...priceRange,
            [e.target.name]: Number(e.target.value),
        });
    };

    const applyPriceRangeFilter = () => {
        filterByPriceRange(priceRange.min, priceRange.max);
    };

    const addToCart = (product: any, quantity: number = 1) => {
        cartStore.addItem(product, quantity);
    };

    const clearCart = () => {
        cartStore.clearItems();
    };

    const selectProductForEdit = (product: any) => {
        setSelectedProduct(product);
    };

    const clearSelectedProduct = () => {
        setSelectedProduct(null);
    };

    const handleSearchChange = (query: string) => {
        setSearchQuery(query);
        if (query === '') {
            setFilteredProducts(products);
        } else {
            const filteredProducts = products.filter(product => 
                product.name.toLowerCase().includes(query.toLowerCase())
            );
            setFilteredProducts(filteredProducts);
        }
    };

    return (
        <Router>
            <div className="container">
                <Header onSearch={handleSearchChange} />
                <Routes>
                    <Route path="/" element={
                        <Catalog
                            products={products}
                            filteredProducts={filteredProducts}
                            priceRange={priceRange}
                            error={error}
                            selectedProduct={selectedProduct}
                            onShowAllProducts={showAllProducts}
                            onFilterByType={filterByType}
                            onFilterByPriceRange={filterByPriceRange}
                            onAddToCart={addToCart}
                            onEditProduct={selectProductForEdit}
                            onRemoveProduct={removeProduct}
                            onUpdateProduct={updateProduct}
                            onClearSelectedProduct={clearSelectedProduct}
                            onPriceRangeChange={handlePriceRangeChange}
                            onApplyPriceRangeFilter={applyPriceRangeFilter}
                        />
                    } />
                    <Route path="/add-product" element={<ProductForm onAddProduct={addProduct} />} />
                    <Route path="/cart" element={<CartList items={cartItems} onRemoveItem={removeCartItem} onClearCart={clearCart} />} />
                    <Route path="/edit-product/:id" element={
                        selectedProduct && (
                            <EditProductForm 
                                product={selectedProduct} 
                                onUpdateProduct={updateProduct} 
                                onClearSelectedProduct={clearSelectedProduct} 
                            />
                        )
                    } />
                </Routes>
                <Footer />
            </div>
        </Router>
    );
};

export default App;
