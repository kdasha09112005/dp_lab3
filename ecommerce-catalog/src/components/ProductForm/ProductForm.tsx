import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { categories } from '../../constants';
import Button from '../Button/Button';
import './ProductForm.css';

interface ProductFormProps {
    onAddProduct: (name: string, price: number, type: string) => void;
}

const ProductForm: React.FC<ProductFormProps> = ({ onAddProduct }) => {
    const [name, setName] = useState('');
    const [price, setPrice] = useState<number | string>('');
    const [type, setType] = useState(categories[0]);
    const navigate = useNavigate();

    const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };

    const handlePriceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPrice(Number(e.target.value));
    };

    const handleTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setType(e.target.value);
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (name && Number(price) > 0 && type) {
            onAddProduct(name, Number(price), type);
            setName('');
            setPrice('');
            setType(categories[0]);
        }
    };

    const handleCancel = () => {
        navigate('/');
    };

    return (
        <div className="product-form-container">
            <form onSubmit={handleSubmit} className="product-form">
                <h2>Add Product</h2>
                <label htmlFor="name">Product Name</label>
                <input
                    id="name"
                    type="text"
                    placeholder="Enter product name"
                    value={name}
                    onChange={handleNameChange}
                />
                <label htmlFor="price">Price</label>
                <input
                    id="price"
                    type="number"
                    placeholder="Enter price"
                    value={price}
                    onChange={handlePriceChange}
                />
                <label htmlFor="type">Category</label>
                <select id="type" value={type} onChange={handleTypeChange}>
                    {categories.map((category) => (
                        <option key={category} value={category}>
                            {category}
                        </option>
                    ))}
                </select>
                <div className="form-buttons">
                    <Button buttonType="primary" size="small" type="submit" text="Add Product" />
                    <Button buttonType="secondary" size="small" onClick={handleCancel} text="Cancel" />
                </div>
            </form>
        </div>
    );
};

export default ProductForm;
