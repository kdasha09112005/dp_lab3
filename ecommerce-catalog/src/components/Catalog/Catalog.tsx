import React from 'react';
import Button from '../Button/Button';
import ProductList from '../ProductList/ProductList';

interface CatalogProps {
    products: any[];
    filteredProducts: any[];
    priceRange: { min: number, max: number };
    error: string | null;
    selectedProduct: any | null;
    onShowAllProducts: () => void;
    onFilterByType: (type: string) => void;
    onFilterByPriceRange: (min: number, max: number) => void;
    onAddToCart: (product: any, quantity: number) => void;
    onEditProduct: (product: any) => void;
    onRemoveProduct: (id: string) => void;
    onUpdateProduct: (id: string, name: string, price: number, type: string) => void;
    onClearSelectedProduct: () => void;
    onPriceRangeChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    onApplyPriceRangeFilter: () => void;
}

const Catalog: React.FC<CatalogProps> = ({
    products,
    filteredProducts,
    priceRange,
    error,
    selectedProduct,
    onShowAllProducts,
    onFilterByType,
    onFilterByPriceRange,
    onAddToCart,
    onEditProduct,
    onRemoveProduct,
    onUpdateProduct,
    onClearSelectedProduct,
    onPriceRangeChange,
    onApplyPriceRangeFilter
}) => {
    return (
        <div>
            <div className="filter-section">
                <div className="filter-buttons">
                    <Button buttonType="primary" size="small" onClick={onShowAllProducts} text="Show All Products" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Smartphones and Accessories')} text="Show Smartphones and Accessories" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Audio Equipment')} text="Show Audio Equipment" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Gaming')} text="Show Gaming" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Computers and Peripherals')} text="Show Computers and Peripherals" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Home Automation')} text="Show Home Automation" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Wearable Technology')} text="Show Wearable Technology" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Cameras and Photography')} text="Show Cameras and Photography" />
                    <Button buttonType="primary" size="small" onClick={() => onFilterByType('Networking')} text="Show Networking" />
                </div>
                <div className="price-range">
                    <input
                        type="number"
                        name="min"
                        value={priceRange.min}
                        onChange={onPriceRangeChange}
                        placeholder="Min Price"
                    />
                    <input
                        type="number"
                        name="max"
                        value={priceRange.max}
                        onChange={onPriceRangeChange}
                        placeholder="Max Price"
                    />
                    <Button buttonType="primary" size="small" onClick={onApplyPriceRangeFilter} text="Show Products by Price Range" />
                </div>
            </div>
            {error && <p className="error-message">{error}</p>}
            <ProductList products={filteredProducts} onAddToCart={onAddToCart} onEditProduct={onEditProduct} onRemoveProduct={onRemoveProduct} />
        </div>
    );
};

export default Catalog;
