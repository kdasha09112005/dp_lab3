import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../Button/Button';
import './ProductList.css';

interface ProductListProps {
    products: any[];
    onAddToCart: (product: any, quantity: number) => void;
    onEditProduct: (product: any) => void;
    onRemoveProduct: (id: string) => void;
}

const ProductList: React.FC<ProductListProps> = ({ products, onAddToCart, onEditProduct, onRemoveProduct }) => {
    const [quantities, setQuantities] = useState<{ [key: string]: number }>({});
    const navigate = useNavigate();

    const handleQuantityChange = (productId: string, quantity: number) => {
        setQuantities({
            ...quantities,
            [productId]: quantity,
        });
    };

    const handleAddToCart = (product: any) => {
        const quantity = quantities[product.id] || 1;
        onAddToCart(product, quantity);
    };

    const handleEditProduct = (product: any) => {
        onEditProduct(product);
        navigate(`/edit-product/${product.id}`);
    };

    const categories = Array.from(new Set(products.map(product => product.type)));

    return (
        <div className="product-list">
            {categories.map((category, categoryIndex) => (
                <div key={categoryIndex} className="category-section">
                                        <h2 className="category-title">{category}</h2>
                    <ul>
                        {products
                            .filter(product => product.type === category)
                            .map(product => (
                                <li key={product.id} className="product-item">
                                    <div>
                                        <span>{product.name} (${product.price.toFixed(2)})</span>
                                    </div>
                                    <div className='product-buttons'>
                                        <input
                                            type="number"
                                            min="1"
                                            value={quantities[product.id] || 1}
                                            onChange={(e) => handleQuantityChange(product.id, parseInt(e.target.value))}
                                            className="quantity-input"
                                        />
                                        <Button buttonType="primary" size="small" onClick={() => handleAddToCart(product)} text="Add to Cart" />
                                        <Button buttonType="secondary" size="small" onClick={() => handleEditProduct(product)} text="Edit" />
                                        <Button buttonType="secondary" size="small" onClick={() => onRemoveProduct(product.id)} text="Remove" />
                                    </div>
                                </li>
                            ))}
                    </ul>
                </div>
            ))}
        </div>
    );
};

export default ProductList;

