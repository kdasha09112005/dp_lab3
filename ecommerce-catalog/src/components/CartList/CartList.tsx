import React from 'react';
import Button from '../Button/Button';
import './CartList.css';

interface CartListProps {
    items: { product: any, quantity: number }[];
    onRemoveItem: (id: string) => void;
    onClearCart: () => void;
}

const CartList: React.FC<CartListProps> = ({ items, onRemoveItem, onClearCart }) => {
    const getTotalPrice = () => {
        return items.reduce((total, item) => total + item.product.price * item.quantity, 0).toFixed(2);
    };

    return (
        <div className="cart-container">
            <div className="cart">
                <h2>Cart</h2>
                {items.length === 0 ? (
                    <p>Your cart is empty.</p>
                ) : (
                    items.map((item, index) => (
                        <div key={index} className="cart-item">
                            <p>{item.product.name} (${item.product.price}) - Quantity: {item.quantity} - Subtotal: ${(item.product.price * item.quantity).toFixed(2)}</p>
                            <Button buttonType="secondary" size="small" onClick={() => onRemoveItem(item.product.id)} text="Remove" />
                        </div>
                    ))
                )}
                <div className="cart-total">
                    <p>Total: ${getTotalPrice()}</p>
                </div>
                {items.length > 0 && (
                    <Button buttonType="primary" size="small" onClick={onClearCart} text="Clear Cart" />
                )}
            </div>
        </div>
    );
};

export default CartList;
