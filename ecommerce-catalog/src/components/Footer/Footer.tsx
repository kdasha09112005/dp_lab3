import React from 'react';
import logo from "../../assets/images/logo.png";
import './Footer.css';

const Footer: React.FC = () => {
    return (
        <footer className="footer">
            <div className='footer-info'>
                <div className="footer-content">
                    <div className="footer-column">
                        <img src={logo} alt="Logo" className="footer-logo" />
                        <p>Your one-stop shop for all tech needs. Discover the latest and greatest in electronics, from smartphones to gaming consoles.</p>
                    </div>
                    <div className="footer-column">
                        <h4>COMPANY</h4>
                        <a href="/about-us">About Us</a>
                        <a href="/careers">Careers</a>
                        <a href="/blog">Blog</a>
                        <a href="/contact">Contact Us</a>
                    </div>
                    <div className="footer-column">
                        <h4>SUPPORT</h4>
                        <a href="/faq">FAQ</a>
                        <a href="/shipping">Shipping</a>
                        <a href="/returns">Returns</a>
                        <a href="/warranty">Warranty</a>
                    </div>
                    <div className="footer-column">
                        <h4>LEGAL</h4>
                        <a href="/privacy-policy">Privacy Policy</a>
                        <a href="/terms-of-service">Terms of Service</a>
                        <a href="/license">License</a>
                    </div>
                </div>
                <div className="footer-bottom">
                    <p>&copy; 2024 TechStore. All rights reserved.</p>
                    <div className="footer-socials">
                        <a href="https://instagram.com"><i className="fab fa-instagram"></i></a>
                        <a href="https://twitter.com"><i className="fab fa-twitter"></i></a>
                        <a href="https://youtube.com"><i className="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
