import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import logo from "../../assets/images/logo.png";
import './Header.css';

const Header: React.FC<{ onSearch: (query: string) => void }> = ({ onSearch }) => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(e.target.value);
        onSearch(e.target.value);
    };

    return (
        <header className="header">
            <img src={logo} alt="Logo" className="header-logo" />
            <NavLink to="/add-product" className="nav-link">Add Product</NavLink>
            <input
                type="text"
                placeholder="Search products..."
                value={searchQuery}
                onChange={handleSearchChange}
                className="search-input"
            />
            <nav className='navigation'>
                <NavLink to="/" className="nav-link">Catalog</NavLink>
                <NavLink to="/cart" className="nav-link">Cart</NavLink>
            </nav>
        </header>
    );
};

export default Header;
