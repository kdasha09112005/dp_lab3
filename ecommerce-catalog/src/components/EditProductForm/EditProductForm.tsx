import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../Button/Button';
import { categories } from '../../constants';
import './EditProductForm.css';

interface EditProductFormProps {
    product: any;
    onUpdateProduct: (id: string, name: string, price: number, type: string) => void;
    onClearSelectedProduct: () => void;
}

const EditProductForm: React.FC<EditProductFormProps> = ({ product, onUpdateProduct, onClearSelectedProduct }) => {
    const [name, setName] = useState(product.name);
    const [price, setPrice] = useState(product.price);
    const [type, setType] = useState(product.type);
    const navigate = useNavigate();

    useEffect(() => {
        setName(product.name);
        setPrice(product.price);
        setType(product.type);
    }, [product]);

    const handleUpdateProduct = () => {
        if (name && price && type) {
            onUpdateProduct(product.id, name, price, type);
            navigate('/');
        }
    };

    const handleCancel = () => {
        onClearSelectedProduct();
        navigate('/');
    };

    return (
        <div className="edit-product-form-container">
            <div className="edit-product-form">
                <h2>Edit Product</h2>
                <input
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder="Product Name"
                />
                <input
                    type="number"
                    value={price}
                    onChange={(e) => setPrice(parseFloat(e.target.value))}
                    placeholder="Price"
                />
                <select value={type} onChange={(e) => setType(e.target.value)}>
                    {categories.map((category) => (
                        <option key={category} value={category}>
                            {category}
                        </option>
                    ))}
                </select>
                <div className="edit-product-form-buttons">
                    <Button buttonType="primary" size="small" onClick={handleUpdateProduct} text="Update Product" />
                    <Button buttonType="secondary" size="small" onClick={handleCancel} text="Cancel" />
                </div>
            </div>
        </div>
    );
};

export default EditProductForm;
