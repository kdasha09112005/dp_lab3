import { IRead } from './interfaces/IRead';
import { IWrite } from './interfaces/IWrite';

export abstract class BaseRepository<T> implements IRead<T>, IWrite<T> {
    protected items: T[] = [];

    public add(item: T): void {
        this.items.push(item);
    }

    public remove(item: T): void {
        this.items = this.items.filter(i => i !== item);
    }

    public getAll(): T[] {
        return this.items;
    }

    public findById(id: string): T | undefined {
        return this.items.find(item => (item as any).id === id);
    }

    public findByName(name: string): T[] {
        return this.items.filter(item => (item as any).name === name);
    }

    public findByType(type: string): T[] {
        return this.items.filter(item => (item as any).type === type);
    }

    public findByPriceRange(min: number, max: number): T[] {
        return this.items.filter(item => (item as any).price >= min && (item as any).price <= max);
    }
}
