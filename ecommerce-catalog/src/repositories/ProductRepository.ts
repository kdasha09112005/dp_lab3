import { BaseRepository } from './BaseRepository';

interface Product {
    id: string;
    name: string;
    price: number;
    type: string;
}

class ProductRepository extends BaseRepository<Product> {
    private static instance: ProductRepository;

    private constructor() {
        super();
    }

    public static getInstance(): ProductRepository {
        if (!ProductRepository.instance) {
            ProductRepository.instance = new ProductRepository();
        }
        return ProductRepository.instance;
    }

    public loadProducts(products: Product[]): void {
        this.items = products;
    }
}

export default ProductRepository;
