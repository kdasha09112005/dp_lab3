export interface IWrite<T> {
    add(item: T): void;
    remove(item: T): void;
}
