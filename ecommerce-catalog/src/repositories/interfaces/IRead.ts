export interface IRead<T> {
    getAll(): T[];
    findById(id: string): T | undefined;
    findByName(name: string): T[];
    findByType(type: string): T[];
    findByPriceRange(min: number, max: number): T[];
}
