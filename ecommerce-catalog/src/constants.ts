export const categories = [
    "Smartphones and Accessories",
    "Audio Equipment",
    "Gaming",
    "Computers and Peripherals",
    "Home Automation",
    "Wearable Technology",
    "Cameras and Photography",
    "Networking"
];
