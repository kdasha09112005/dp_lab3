class Product {
    id: string;
    name: string;
    price: number;
    type: string;

    constructor(id: string, name: string, price: number, type: string) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
    }
}

class ProductFactory {
    private static idCounter = 0;

    public static createProduct(type: string, name: string, price: number): Product {
        this.idCounter++;
        const id = this.idCounter.toString();

        switch (type) {
            case "Smartphones and Accessories":
            case "Audio Equipment":
            case "Gaming":
            case "Computers and Peripherals":
            case "Home Automation":
            case "Wearable Technology":
            case "Cameras and Photography":
            case "Networking":
                return new Product(id, name, price, type);
            default:
                throw new Error("Unknown product type");
        }
    }
}

export default ProductFactory;
