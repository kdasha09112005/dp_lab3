import { Observer } from './ObservableBase';
import CartStore from './CartStore';

export class CartObserver implements Observer {
    private updateCallback: () => void;

    constructor(updateCallback: () => void) {
        this.updateCallback = updateCallback;
    }

    public update(observable: CartStore): void {
        this.updateCallback();
    }
}

export default CartObserver;
