import { ObservableBase } from './ObservableBase';

interface CartItem {
    product: any;
    quantity: number;
}

export class CartStore extends ObservableBase {
    private static instance: CartStore;
    private items: CartItem[] = [];

    private constructor() {
        super();
    }

    public static getInstance(): CartStore {
        if (!CartStore.instance) {
            CartStore.instance = new CartStore();
        }
        return CartStore.instance;
    }

    public addItem(product: any, quantity: number = 1) {
        const existingItem = this.items.find(item => item.product.id === product.id);
        if (existingItem) {
            existingItem.quantity += quantity;
        } else {
            this.items.push({ product, quantity });
        }
        this.notifyObservers();
    }

    public removeItem(id: string) {
        this.items = this.items.filter(item => item.product.id !== id);
        this.notifyObservers();
    }

    public clearItems() {
        this.items = [];
        this.notifyObservers();
    }

    public getItems() {
        return this.items;
    }
}

export default CartStore;
